// NOTE: all vars and functions names are prefixed with a unique project-dependent prefix ('bi') to avoid conflicts with other scripts. 

// Sites
const BI_SITES = [
    { name: 'Vegan Pratique', domain: 'vegan-pratique.fr' },
    { name: 'VegOresto', domain: 'vegoresto.fr' },
    { name: 'Politique & Animaux', domain: 'www.politique-animaux.fr'},
    { name: 'Education', domain: 'education.l214.com'},
    { name: 'Improved', domain: 'improved-food.com'},
    { name: 'Viande info', domain: 'www.viande.info'},
    { name: 'Boutique', domain: 'boutique.l214.com'},
];

// Responsive breakpoints
const BI_BREAKPOINTS = [
    {name: 'xs' , value: '319'},
    {name: 'sm' , value: '375'},
    {name: 'lg' , value: '991'}
];

// UTM
BI_UTM_CAMPAIGN = '--utm-campaign--';
BI_UTM_MEDIUM = 'intersite';
BI_UTM_SOURCE = 'banner';

/** HTML ***********/

const biHtml_begin =  `
<div id="js-intersite" class="intersite">
	<div class="l214">
		<a class="l214__link" href="https://www.l214.com"  target="_blank">L214</a>
		<ul class="l214__social">
			<li class="l214__social__item">
				<a class="l214__social__link" href="https://www.facebook.com/l214.animaux" target="_blank">
					<svg class="l214__social__icon" width="18" height="18" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 28" aria-hidden="true">
						<title>Facebook</title>
						<path fill="#ffffff" d="M14.984 0.187v4.125h-2.453c-1.922 0-2.281 0.922-2.281 2.25v2.953h4.578l-0.609 4.625h-3.969v11.859h-4.781v-11.859h-3.984v-4.625h3.984v-3.406c0-3.953 2.422-6.109 5.953-6.109 1.687 0 3.141 0.125 3.563 0.187z"></path>
					</svg>
					<span class="l214__social__icon__label sr-only">Facebook</span>	
				</a>
			</li>
			<li class="l214__social__item">
				<a class="l214__social__link" href="https://twitter.com/l214" target="_blank">
					<svg class="l214__social__icon" width="18" height="18" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 28" aria-hidden="true">
						<title>Twitter</title>
						<path fill="#ffffff" d="M25.312 6.375c-0.688 1-1.547 1.891-2.531 2.609 0.016 0.219 0.016 0.438 0.016 0.656 0 6.672-5.078 14.359-14.359 14.359-2.859 0-5.516-0.828-7.75-2.266 0.406 0.047 0.797 0.063 1.219 0.063 2.359 0 4.531-0.797 6.266-2.156-2.219-0.047-4.078-1.5-4.719-3.5 0.313 0.047 0.625 0.078 0.953 0.078 0.453 0 0.906-0.063 1.328-0.172-2.312-0.469-4.047-2.5-4.047-4.953v-0.063c0.672 0.375 1.453 0.609 2.281 0.641-1.359-0.906-2.25-2.453-2.25-4.203 0-0.938 0.25-1.797 0.688-2.547 2.484 3.062 6.219 5.063 10.406 5.281-0.078-0.375-0.125-0.766-0.125-1.156 0-2.781 2.25-5.047 5.047-5.047 1.453 0 2.766 0.609 3.687 1.594 1.141-0.219 2.234-0.641 3.203-1.219-0.375 1.172-1.172 2.156-2.219 2.781 1.016-0.109 2-0.391 2.906-0.781z"></path>
					</svg>
					<span class="l214__social__icon__label sr-only">Twitter</span>	
				</a>
			</li>
			<li class="l214__social__item">
				<a class="l214__social__link" href="https://www.instagram.com/association_l214" target="_blank">
					<svg class="l214__social__icon" width="18" height="18" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 28" aria-hidden="true">
						<title>Instagram</title>
						<path fill="#ffffff" d="M16 14c0-2.203-1.797-4-4-4s-4 1.797-4 4 1.797 4 4 4 4-1.797 4-4zM18.156 14c0 3.406-2.75 6.156-6.156 6.156s-6.156-2.75-6.156-6.156 2.75-6.156 6.156-6.156 6.156 2.75 6.156 6.156zM19.844 7.594c0 0.797-0.641 1.437-1.437 1.437s-1.437-0.641-1.437-1.437 0.641-1.437 1.437-1.437 1.437 0.641 1.437 1.437zM12 4.156c-1.75 0-5.5-0.141-7.078 0.484-0.547 0.219-0.953 0.484-1.375 0.906s-0.688 0.828-0.906 1.375c-0.625 1.578-0.484 5.328-0.484 7.078s-0.141 5.5 0.484 7.078c0.219 0.547 0.484 0.953 0.906 1.375s0.828 0.688 1.375 0.906c1.578 0.625 5.328 0.484 7.078 0.484s5.5 0.141 7.078-0.484c0.547-0.219 0.953-0.484 1.375-0.906s0.688-0.828 0.906-1.375c0.625-1.578 0.484-5.328 0.484-7.078s0.141-5.5-0.484-7.078c-0.219-0.547-0.484-0.953-0.906-1.375s-0.828-0.688-1.375-0.906c-1.578-0.625-5.328-0.484-7.078-0.484zM24 14c0 1.656 0.016 3.297-0.078 4.953-0.094 1.922-0.531 3.625-1.937 5.031s-3.109 1.844-5.031 1.937c-1.656 0.094-3.297 0.078-4.953 0.078s-3.297 0.016-4.953-0.078c-1.922-0.094-3.625-0.531-5.031-1.937s-1.844-3.109-1.937-5.031c-0.094-1.656-0.078-3.297-0.078-4.953s-0.016-3.297 0.078-4.953c0.094-1.922 0.531-3.625 1.937-5.031s3.109-1.844 5.031-1.937c1.656-0.094 3.297-0.078 4.953-0.078s3.297-0.016 4.953 0.078c1.922 0.094 3.625 0.531 5.031 1.937s1.844 3.109 1.937 5.031c0.094 1.656 0.078 3.297 0.078 4.953z"></path>
					</svg>
					<span class="l214__social__icon__label sr-only">Instagram</span>	
				</a>
			</li>
		</ul>
	</div>
	<div class="sites-toggle" id="js-sites-toggle">
		<button href="#" id="js-sites-toggle-link" class="sites-toggle__link">Découvrir nos sites</button>
		<button href="#" id="js-sites-toggle-close" class="sites-toggle__close is-hidden">
			<svg class="l214__social__icon" width="12" height="12" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 28" aria-hidden="true">
				<title>Fermer</title>
				<path fill="#ffffff" d="M17.953 17.531c0-0.266-0.109-0.516-0.297-0.703l-2.828-2.828 2.828-2.828c0.187-0.187 0.297-0.438 0.297-0.703s-0.109-0.531-0.297-0.719l-1.406-1.406c-0.187-0.187-0.453-0.297-0.719-0.297s-0.516 0.109-0.703 0.297l-2.828 2.828-2.828-2.828c-0.187-0.187-0.438-0.297-0.703-0.297s-0.531 0.109-0.719 0.297l-1.406 1.406c-0.187 0.187-0.297 0.453-0.297 0.719s0.109 0.516 0.297 0.703l2.828 2.828-2.828 2.828c-0.187 0.187-0.297 0.438-0.297 0.703s0.109 0.531 0.297 0.719l1.406 1.406c0.187 0.187 0.453 0.297 0.719 0.297s0.516-0.109 0.703-0.297l2.828-2.828 2.828 2.828c0.187 0.187 0.438 0.297 0.703 0.297s0.531-0.109 0.719-0.297l1.406-1.406c0.187-0.187 0.297-0.453 0.297-0.719zM24 14c0 6.625-5.375 12-12 12s-12-5.375-12-12 5.375-12 12-12 12 5.375 12 12z"></path>
			</svg>
			<span class="l214__social__icon__label">Fermer</span>
		</button>
	</div>
	<ul class="sites" id="js-sites">
`;

const biHtml_end = '</ul></div>';

/** HTML:END ***********/

/** CSS ****************/
let biCSS = `
#js-ban-l214 {
    position: relative;
    width: 100%;
    background-color: #fea800;
    z-index: 9999;
}

.intersite {
    display: flex;
    align-items: stretch;
}

/* L214 and social networks*/
.intersite .l214 {
    position:relative;
    display: flex;
    align-items: center;

    padding: 0 12px;
    margin-right: 7px;

    background: #ea791d;
}

.intersite .l214::after {
    content: "";

    position: absolute;
    right: -5px;
    width: 0;
    height: 0;

    border-style: solid;
    border-width: 7.5px 0 7.5px 6px;
    border-color: transparent transparent transparent #ea791d;
}

.intersite .l214__link {
    font-family: inherit;
    font-weight: 700;
    text-decoration: none;
    color: #fff;

    margin-right: 20px;
}

.intersite .l214__social {
    display: flex;
    list-style: none;

    margin: 0;
    padding: 0;
}

.intersite .l214__social__item {
    line-height: 0;
}

.intersite .l214__social__item:not(:last-child) {
    padding-right: 10px;
}

.intersite .l214__social__link {
    display:block;
    color: #fff;
}

.intersite .l214__social__icon__label {
    padding-left: 3px;
}
.intersite .l214__social__icon__label.sr-only {
    position: absolute;
    width: 1px;
    height: 1px;
    padding: 0;
    overflow: hidden;
    clip: rect(0, 0, 0, 0);
    white-space: nowrap;
    border: 0;
}

.intersite .l214 .l214__social__icon{
    opacity: 0.7;
}
.intersite .l214 .l214__social__icon:hover{
    opacity: .9;
}

/* Sites toggle */
.intersite .sites-toggle {
    display: flex;
    align-items: center;
    justify-content: center;
    flex: auto;

    text-transform: uppercase;
}

.intersite .sites-toggle__link,
.intersite .sites-toggle__close {
    font-size: .85rem;
    font-weight: 700;

    background-color: #fea800;
    color: #fff;

    text-decoration: none;
    border: 0;
    cursor: pointer;
}

.intersite .sites-toggle__close {
    display: flex;
    align-items: center;
}

.intersite .sites-toggle__link.is-hidden,
.intersite .sites-toggle__close.is-hidden {
    display: none;
}

/* Sites */
.intersite .sites {
    display: none;

    position: absolute;
    width: 100%;
    left: 0;
    top: 100%;

    background: #fea800;

    list-style: none;
    margin: 0;
    padding: 0;
    line-height: 0;

    z-index: 999;
}

.intersite .sites.is-open {
    display: flex;
    flex-direction: column;
}

.intersite .sites__item {
    position: relative;
    width: 100%;
    height:auto;

    text-align: center;

    padding: 15px 0px;
    line-height: 1;

    border-top: solid 1px rgba(255,255,255,0.2);
}

.intersite .sites__link,
.intersite .sites__label {
    display: inline-block;
    font-family: inherit;
    font-size: 0.8rem;
    font-weight: 400;
    color: white;
    text-transform: uppercase;
    text-decoration: none;
}

.intersite .sites__item.is-current .sites__label,
.intersite .sites__item.is-current .sites__link {
    font-weight: 700;
}

/* Sites toggle - collapsed */
@media screen and (min-width: --brk-sm--px) {
    .intersite .sites-toggle__link,
    .intersite .sites-toggle__close {
        font-size: 1rem;
    }
}

@media screen and (min-width: --brk-lg--px) {
    .intersite {
        border: none;
    }

    /* Sites */
    .intersite .sites {
        position:relative;
        width: auto;
        left:auto;
        top:auto;

        display: flex;
        flex: auto;
        flex-direction: row!important;
        align-items: center;

        border: none;
    }

    .intersite .sites__item {
        width: auto;
        height:auto;
        text-align: left;

        margin: 4px 0;
        padding: 0px 10px;

        border: 0;
    }

    .intersite .sites__item:not(:last-child) {
        border: none;
        border-right: solid #fff 1px;
    }

    .intersite .sites__item.is-current:after {
        content: "";
        position: absolute;
        width: 0;
        height: 0;
        bottom: -5px;
        left: calc(50% - 2px);

        border-left: 4px solid transparent;
        border-right: 4px solid transparent;
        border-bottom: 4px solid #fff;
    }

    .intersite .sites__link:hover {
        text-decoration: underline;
    }

    /* Sites toggle - collapsed */
    .intersite .sites-toggle {
        display: none;
    }
}
`
/** CSS:END ************/

function biL214() {
    let banWidget = document.getElementById("js-ban-l214");
    if (banWidget) {
        const source = banWidget.dataset.source || 'L214';
        const source_domain  = banWidget.dataset.domain || window.location.hostname;
        const isL214 = (source_domain === 'l214.com');
        const justifyRight = banWidget.dataset.justify ? banWidget.dataset.justify === 'right' : false;

        // Build custom HTML
        let banHtml = biHtml_begin;
        BI_SITES.forEach(element => {
            const isCurrent = source_domain === element['domain'];
            banHtml += '<li class="sites__item' + (isCurrent ? ' is-current': '') + '">';
            if (!isCurrent) {
                banHtml += `<a class="sites__link" href="https://${element['domain']}?utm_campaign=${BI_UTM_CAMPAIGN}&utm_medium=${BI_UTM_MEDIUM}&utm_source=${BI_UTM_SOURCE}" target="_blank">${element['name']}</a>`;
            } else {
                banHtml += `<span class="sites__label">${element['name']}</span>`;
            }
            banHtml += '</li>';
        });
        banHtml += biHtml_end;
        banWidget.innerHTML = banHtml.replace(/--utm-campaign--/g, source);

        // Custom CSS
        if (isL214) {
            biCSS += '.intersite .l214 {display:none;}';
        }
        if (justifyRight) {
            biCSS += '@media screen and (min-width: --brk-lg--px) {.intersite .sites {justify-content:flex-end}}';
        }
        BI_BREAKPOINTS.forEach(element => {
            const replace = `--brk-${element['name']}--`;
            const regexp = new RegExp(replace, "g");
            biCSS = biCSS.replace(regexp, element['value']);
        });
        biJslAddCss(biCSS)

        // Open/Close on small screens
        if (document.getElementById('js-intersite')) {
            function biToggle(force='no')
            {
                const intersites = document.getElementById('js-sites');
                if (!intersites) {
                    return false;
                }
                switch (force) {
                    case 'open':
                        intersites.classList.add('is-open');
                        break;
                    case 'close':
                        intersites.classList.remove('is-open');
                        break;
                    default:
                        intersites.classList.toggle('is-open');
                        break;
                }
                if (intersites.classList.contains('is-open')) {
                    document.getElementById('js-sites-toggle-link').classList.add('is-hidden');
                    document.getElementById('js-sites-toggle-close').classList.remove('is-hidden');
                } else {
                    document.getElementById('js-sites-toggle-link').classList.remove('is-hidden');
                    document.getElementById('js-sites-toggle-close').classList.add('is-hidden');
                }
            }
            // Toogle drop down list on click
            const intersite_toggle = document.getElementById('js-sites-toggle');
            if (intersite_toggle) {
                intersite_toggle.addEventListener('click', function(event) {
                    biToggle();
                })
            }
            // Close dropdown list on external click
            window.addEventListener('click', function(event) {
                if (event.target.closest('#js-intersite') === false) {
                    biToggle('close');
                }
            });
            // Close dropdown list on scroll
            window.addEventListener('scroll', function(event) {
                const intersites = document.getElementById('js-sites');
                if (intersites && intersites.classList.contains('is-open')) {
                    biToggle('close');
                }
            });
        }
    }
}

try {
    document.addEventListener("DOMContentLoaded", function(event) {
        biL214();
    });
} catch (err) {
  biSentryCapture(err.message);
  console.log(err.message);
}

/* *********************** */
/* ** HELPERS ************ */
/* *********************** */
function biJslAddCss(cssCode) {
    var styleElement = document.createElement("style");
    styleElement.type = "text/css";
    if (styleElement.styleSheet) {
        styleElement.styleSheet.cssText = cssCode;
    } else {
        styleElement.appendChild(document.createTextNode(cssCode));
    }
    document.getElementsByTagName("head")[0].appendChild(styleElement);
}

function biSentryCapture(message) {
  if (typeof Sentry !== "undefined") {
    Sentry.captureException(message);
  }
}
